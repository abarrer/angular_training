const http = require("http");
const path = require("path");
var fs = require('fs');
var request = require("request");

const port = 5000;

http.createServer(function (request, response) {
    console.log('Requested: '+ request.url);
    var file_to_load = request.url.replace("/","");
    if(file_to_load == ""){
    file_to_load = "index.html"
    }
    
    var readStream = fs.createReadStream(path.resolve(__dirname, file_to_load));
    
    readStream.on('open', function () {
    readStream.pipe(response);
    });
    
    readStream.on('error', function(err) {
    response.end();
    });
    }
).listen(port);