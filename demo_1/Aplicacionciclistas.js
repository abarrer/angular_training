﻿angular.module('aplicacionCiclistas', ['ngRoute']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
        when('/ciclistas', {
            templateUrl: 'Codigo/Vistas/Listado.html',
            controller: ListadoController
        }).
        when('/ciclista/:ciclistaId', {
            templateUrl: 'Codigo/Vistas/Palmares.html',
            controller: DetalleCiclistasController
        }).
        otherwise({ redirectTo: '/ciclistas' });
    }]);