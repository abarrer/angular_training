﻿function ListadoController($http, $scope) {
    $http.get('Datos/ciclistas.json').success(function (ListadoDeLosCiclistas) {
        $scope.ciclistas = ListadoDeLosCiclistas;
    });
    $scope.campoOrdenacion = 'Nombre';
    $scope.direccion = false;
}