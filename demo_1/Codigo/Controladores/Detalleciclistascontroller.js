﻿function DetalleCiclistasController( $http, $scope, $routeParams ) {
    $scope.id = $routeParams.ciclistaId;

    $http.get('Datos/ciclista' + $scope.id + '.json').success(function (DatosLeidos) {
        $scope.ciclista = DatosLeidos[0];
    });
}